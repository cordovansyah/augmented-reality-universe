# Augmented Reality Universe

Augmented Reality Universe is an ARKit technology-based iOS app that provides stellary experience about planetary orbital rotation and revolution 
and more into general astronomical knowledge that turns into real life

# Motivation
Astronomy, space, rockets, universe, black hole, String Theory, event horizon, and any stellar objects are actually my deepest favored subjects to learn. 
I even took my very first MOOC course from Harvard at edx and it was Extrasolar Biolife which unfolds deeper knowledge about how living things evolve in Earth
that has string correlation towards stellar collision in the distance past. I took my - well I haven't finish it yet - second MOOC course which was Astrophysics
from MITx at eDX, which unfolds in-depth knowledge about unmanned to manned mission to space, satellite structure and all of that. I love space, it makes me fall
deeper in love with the great divine power and how everything seems connected to us, in both atomic to quark size. 

Then, when I discovered the possibility of bringing an immersive experience of astronomy knowledge through ARKit 2.0 in iOS development journey. I was flabbergasted
and jumped right in to learn and build one, and this project underlines that.

## Why I Build This App?
Curiosity. I watched WWDC 18 and learn several things regarding several Machine Learning libraries and seeing the Apple team's demonstration
of how easy it is to incorporate ML model to iOS app where non-ML engineers can focus in delivering better user experience, or so called intelligent
user experience made myself, flabbergasted. I am currently work as a UX Engineer, and my curiosity comes out of blue about how ML could work in unison
alongisde UX and finally I got the answer, and decided to focus on building more AI related app prototypes, specifically enhancing it's user experience


### Motivation

### Prerequisites
```
Xcode 10.0
Swift 4.0 / 4.2 
iOS 12
```

## Built With

* [MNIST Digit Classifier](http://yann.lecun.com/exdb/mnist/) - CoreML Model Used
* [CoreML Model Libraries](https://coreml.store/) - CoreML Model Used


## Acknowledgments
* Amazing CoreML Model Libraries !! (https://coreml.store/)

